import { useState } from "react";
import { AuthContext } from "./AuthContext";
import { User } from "../models/User";

export default function AuthContextProvider({children}){

const [user, setUser] = useState(new User("", "", "", []))

const [isAuth, setIsAuth]= useState(false)

const [isAdmin, setIsAdmin]= useState(false)

return(
    <>
        <AuthContext.Provider value={{user, setUser, isAuth, setIsAuth, isAdmin, setIsAdmin}}>
            {children}
        </AuthContext.Provider>
    </>
)

}