import { combineReducers } from "redux";
import { counterSlice } from "./example";
import { loginSlice } from "./login/loginReducer";

export default combineReducers({    
    esempio: counterSlice.reducer,
    login: loginSlice.reducer
});
