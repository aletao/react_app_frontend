import {createSlice} from '@reduxjs/toolkit';
import { User } from '../../../models/User';

const loggedUser = {
    token: "",
    nome: "",
    cognome: "",
    email: "",
    ruoli: []
}

export const loginSlice = createSlice({
    name: 'login',
    initialState: loggedUser,
    reducers: {
        setToken: (state, action) => {
            state.token =  action.payload;
        },
        setNome: (state, action) => {
            state.nome = action.payload;
        },
        setCognome: (state, action) => {
            state.cognome = action.payload;
        },
        setEmail: (state, action) => {
            state.email = action.payload;
        },
        setRuoli: (state, action) => {
            state.ruoli = action.payload;
        }
    }
});

export const {setToken, setNome, setCognome, setEmail, setRuoli} = loginSlice.actions;