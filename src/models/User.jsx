export class User {
    constructor(nome, cognome, email, ruoli) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.ruoli = ruoli;
    }
}