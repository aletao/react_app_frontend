export class Course{
    constructor(id, nomeCorso, descrizione, durata, categoria){
        this.id = id;
        this.nomeCorso = nomeCorso;
        this.descrizione = descrizione;
        this.durata = durata;
        this.categoria = categoria;
    }
}