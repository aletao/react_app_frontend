import {useOutlet} from "react-router-dom"
import Navbar from "../components/Navbar/Navbar"
import Footer from "../components/Footer/Footer"

export default function Navigation(){
    const outlet = useOutlet()

    return(
        <>
        <Navbar/>
        {outlet}
        <Footer/>
        </>
    )
}