import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import AuthContextProvider from "./contexts/AuthContextProvider.jsx";
import HomePage from "./pages/HomePage/HomePage.jsx";
import LoginPage from "./pages/LoginPage/LoginPage.jsx";
import Navigation from "./layouts/Navigation.jsx";
import RegisterPage from "./pages/RegisterPage/RegisterPage.jsx";
import MyProfile from "./pages/My-Profile/MyProfile.jsx";
import AdminPanel from "./pages/AdminPanel/AdminPanel.jsx";
import Courses from "./pages/Courses/Courses.jsx";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import store, { persistStorage } from "./Store/index.jsx";
import PrivateRoute from "./Shared/PrivateRoute.jsx";

const router = createBrowserRouter([
  {
    element: (
      <AuthContextProvider>
        <Navigation />
      </AuthContextProvider>
    ),
    children: [
      {
        path: "/",
        element: <HomePage />,
      },
      {
        path: "/login",
        element: <LoginPage />,
      },
      {
        path: "/register",
        element: <RegisterPage />,
      },
      {
        path: "",
        element: <PrivateRoute />,
        children: [
          {
            path: "/myprofile",
            element: <MyProfile />,
          },
          {
            path: "/adminpanel",
            element: <AdminPanel />,
          },
          {
            path: "/courses",
            element: <Courses />,
          },
        ],
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistStorage}>
      <RouterProvider router={router} />
    </PersistGate>
  </Provider>
);
