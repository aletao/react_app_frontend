import styles from "./Courses.module.css";
import { useState, useEffect } from "react";
import { getAllCorsi, deleteCourse } from "../../service/CorsiService";
import { useSelector } from "react-redux";
import { Course } from "../../models/Course";
import { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";

export default function Courses() {
  const loggedUser = useSelector((state) => state.login);

  const { isAdmin } = useContext(AuthContext);

  const { isAuth } = useContext(AuthContext);

  const { isFormVisible, setIsFormVisible } = useState(false);

  const [courses, setCourses] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Form submitted");
  };

  const handleDeleteCourse = async (id) => {
    const response = await deleteCourse(id, loggedUser.token);
    if (response.status === 200) {
      console.log("Corso eliminato");
      getAllCourses();
    }
  };

  const getAllCourses = async () => {
    const response = await getAllCorsi(loggedUser.token);
    const tempCourses = [];
    response.map((element) => {
      tempCourses.push(
        new Course(
          element.id,
          element.nomeCorso,
          element.descrizioneBreve,
          element.durata,
          element.categoria.nomeCategoria
        )
      );
    });
    console.log(tempCourses);
    setCourses(tempCourses);
  };

  useEffect(() => {
    getAllCourses();
  }, []);

  return (
    <>
      <div className={styles.topDiv}>
        <h1>Scopri i nostri corsi</h1>
      </div>
      {isAdmin && (
        <div style={{ textAlign: "center", marginBottom: "2vh" }}>
          <div className="d-grid gap-2 col-2 mx-auto">
            <button className="btn btn-primary" type="button">
              Aggiungi un corso
            </button>
          </div>
        </div>
      )}
      <div style={{ height: 80 + "vh", textAlign: "center" }}>
        <div className="container">
          <div className="row">
            {courses.map((course, index) => (
              <div key={index} className="col-lg-4 col-md-6 mb-4">
                <div className="card h-100">
                  <div className="card-body">
                    <h4 className="card-title">
                      {course.nomeCorso} - {course.categoria}
                    </h4>
                    <p className="card-text">{course.descrizione}</p>
                    <p className="card-text">Durata: {course.durata} mesi</p>
                  </div>
                  <div className="card-footer">
                    <button className="btn btn-primary">Iscriviti</button>
                    {isAdmin && (
                      <button
                        className="btn btn-danger"
                        onClick={() => handleDeleteCourse(course.id)}
                      >
                        Elimina
                      </button>
                    )}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
