import styles from "./AdminPanel.module.css";
import { getAllUsers } from "../../service/UserService";
import { User } from "../../models/User";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { deleteUser, updateUser } from "../../service/UserService";

export default function AdminPanel() {
  const loggedUser = useSelector((state) => state.login);

  const [users, setUsers] = useState([]);

  const [isFormVisible, setIsFormVisible] = useState(false);

  const [userToEdit, setUserToEdit] = useState({});

  const handleDeleteUser = async (email) => {
    const response = await deleteUser(email, loggedUser.token);
    console.log(response);
    handleGetAllUsers();
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(userToEdit);
    setIsFormVisible(false);
    const response = await updateUser(userToEdit, loggedUser.token);
    console.log(response);
    handleGetAllUsers();
  };
    

  const handleEditUser = (user) => {
    setUserToEdit(user);
    setIsFormVisible(true);
  };

  const handleGetAllUsers = async () => {
    const response = await getAllUsers(loggedUser.token);
    const tempUsers = [];
    response.map((element) => {
      tempUsers.push(
        new User(element.nome, element.cognome, element.email, element.ruoli)
      );
    });
    console.log(tempUsers);
    setUsers(tempUsers);
  };

  useEffect(() => {
    handleGetAllUsers();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserToEdit((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  };

  return (
    <>
      <div className={styles.topPanel}>
        <h1>Admin Panel</h1>
        <div className="container">
          <div className="row">
            <div className="col-md">
              <table className="table table-dark table-hover table-bordered rounded">
                <thead>
                  <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Cognome</th>
                    <th scope="col">Email</th>
                    <th scope="col">Ruoli</th>
                    <th scope="col">Elimina </th>
                    <th scope="col">Modifica</th>
                  </tr>
                </thead>
                <tbody>
                  {users.map((user, index) => (
                    <tr key={index}>
                      <td>{user.nome}</td>
                      <td>{user.cognome}</td>
                      <td>{user.email}</td>
                      <td>
                        {user.ruoli.map((ruolo, index) => (
                          <span key={index}>{ruolo.tipoRuolo} </span>
                        ))}
                      </td>
                      <td>
                        <button type="button" className="btn btn-outline-danger">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            className="bi bi-trash"
                            viewBox="0 0 16 16"
                            onClick={() => handleDeleteUser(user.email)}
                          >
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"></path>
                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"></path>
                          </svg>
                        </button>
                      </td>
                      <td>
                        <button
                          type="button"
                          className="btn btn-outline-primary"
                          onClick={() => handleEditUser(user)}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            className="bi bi-brush"
                            viewBox="0 0 16 16"
                          >
                            <path d="M15.825.12a.5.5 0 0 1 .132.584c-1.53 3.43-4.743 8.17-7.095 10.64a6.1 6.1 0 0 1-2.373 1.534c-.018.227-.06.538-.16.868-.201.659-.667 1.479-1.708 1.74a8.1 8.1 0 0 1-3.078.132 4 4 0 0 1-.562-.135 1.4 1.4 0 0 1-.466-.247.7.7 0 0 1-.204-.288.62.62 0 0 1 .004-.443c.095-.245.316-.38.461-.452.394-.197.625-.453.867-.826.095-.144.184-.297.287-.472l.117-.198c.151-.255.326-.54.546-.848.528-.739 1.201-.925 1.746-.896q.19.012.348.048c.062-.172.142-.38.238-.608.261-.619.658-1.419 1.187-2.069 2.176-2.67 6.18-6.206 9.117-8.104a.5.5 0 0 1 .596.04M4.705 11.912a1.2 1.2 0 0 0-.419-.1c-.246-.013-.573.05-.879.479-.197.275-.355.532-.5.777l-.105.177c-.106.181-.213.362-.32.528a3.4 3.4 0 0 1-.76.861c.69.112 1.736.111 2.657-.12.559-.139.843-.569.993-1.06a3 3 0 0 0 .126-.75zm1.44.026c.12-.04.277-.1.458-.183a5.1 5.1 0 0 0 1.535-1.1c1.9-1.996 4.412-5.57 6.052-8.631-2.59 1.927-5.566 4.66-7.302 6.792-.442.543-.795 1.243-1.042 1.826-.121.288-.214.54-.275.72v.001l.575.575zm-4.973 3.04.007-.005zm3.582-3.043.002.001h-.002z" />
                          </svg>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      {isFormVisible && (
      <div className={styles.editUser} id="edit-user">
        <h1>Modifica utente</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Email address
            </label>
            <input
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              readOnly={true}
              disabled={true}
              name="email"
              value={userToEdit.email}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Nome
            </label>
            <input
              type="text"
              className="form-control"
              id="exampleInputPassword1"
              onChange={handleChange}
              name="nome"
              value={userToEdit.nome}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Cognome
            </label>
            <input
              type="text"
              className="form-control"
              id="exampleInputPassword1"
              name="cognome"
              value={userToEdit.cognome}
              onChange={handleChange}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Modifica
          </button>
        </form>
      </div>
      )}
    </>
  );
}
