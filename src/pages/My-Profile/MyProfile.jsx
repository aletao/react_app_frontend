import styles from "./MyProfile.module.css";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";

export default function MyProfile() {

    const loggedUser = useSelector((state) => state.login);

    const [courses, setCourses] = useState([]);


  return (
    <>
      <div className={styles.topDiv}>
        <h1>Profilo Utente</h1>
      </div>

      <div className={styles.topProfile}>
        <div className="card" style={{ width: "18rem" }}>
          <img src="./profile.jpg" className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{loggedUser.nome} {loggedUser.cognome}</h5>
            <span>Email: {loggedUser.email}</span>
            <span></span>
            
          </div>
        </div>
      </div>


    </>
  );
}
