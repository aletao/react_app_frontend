import styles from "./RegisterPage.module.css";
import { RegisterUser } from "../../models/RegisterUser";
import { userRegister } from "../../service/AuthService";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function RegisterPage() {
  const navigate = useNavigate();
  const [registerUser, setRegisterUser] = useState(
    new RegisterUser("", "", "", "")
  );

  const handleChange = (e) => {
    const { name, value } = e.target;
    setRegisterUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const response = await userRegister(registerUser);
    if (response.status === 200) {
      navigate("/login");
    }
  };

  const handleLoginClick = () => {
    navigate("/login");
  };

  return (
    <>
      <div className={styles.centerDiv}>
        <div className={styles.formContainer}>
          <img src="./beeloan.png" alt="logo" className={styles.imageStyle}/>
          <form onSubmit={handleSubmit}>
            <div className={styles.firstSection}>
              <div>
                <label htmlFor="nome" className="form-label">
                  Nome
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="nome"
                  name="nome"
                  onChange={handleChange}
                  aria-describedby="emailHelp"
                  required={true}
                />
              </div>
              <div>
                <label htmlFor="cognome" className="form-label">
                  Cognome
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="cognome"
                  name="cognome"
                  onChange={handleChange}
                  aria-describedby="emailHelp"
                  required={true}
                />
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor="email" className="form-label">
                Email address
              </label>
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                onChange={handleChange}
                aria-describedby="emailHelp"
                required={true}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <input
                type="password"
                className="form-control"
                id="password"
                name="password"
                onChange={handleChange}
              />
            </div>
            <div id="passwordHelp" className="form-text">
              Password almeno 8 caratteri e un carattere speciale
            </div>
            <div>
              <span onClick={handleLoginClick}>Login</span>
            </div>
            <button type="submit" className="btn btn-primary">
              Registrati
            </button>
          </form>
        </div>
      </div>
    </>
  );
}
