import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";
import styles from "./HomePage.module.css";

export default function HomePage() {
  
  const { user, setUser } = useContext(AuthContext);

  const { isAuth, setIsAuth } = useContext(AuthContext);

  const navigate = useNavigate();

  return (
    <>
      <div className={styles.topDiv}>
        <h1 >Entra a far parte della nostra community.</h1>
      </div>
      <div className={styles.centerDiv}>
        <h2>Trova il corso giusto che fa per te.</h2>
        <div className={styles.cardCategories}>
          <div className={styles.courseCategory}>
            <div className="card mb-3" style={{ maxWidth: 800 }}>
              <div className="row g-0">
                <div className="col-md-4">
                  <img
                    src="./fullstack.jpg"
                    className="img-fluid rounded-start"
                    alt="..."
                  />
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">Full-Stack Dev</h5>
                    <p className="card-text">
                      Esplora sia il frontend che il backend per creare
                      applicazioni web dinamiche e funzionali. Integrando HTML,
                      CSS e JavaScript con linguaggi server-side, imparerai a
                      costruire soluzioni complete.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.courseCategory}>
            <div className="card mb-3" style={{ maxWidth: 800 }}>
              <div className="row g-0">
                <div className="col-md-4">
                  <img
                    src="./frontend.webp"
                    className="img-fluid rounded-start"
                    alt="..."
                  />
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">Front-End Dev</h5>
                    <p className="card-text">
                      Padronanza di HTML, CSS e JavaScript per creare esperienze
                      utente coinvolgenti. Esplora framework come React e Vue.js
                      e ottimizza le performance delle tue applicazioni web.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.courseCategory}>
            <div className="card mb-3" style={{ maxWidth: 800 }}>
              <div className="row g-0">
                <div className="col-md-4">
                  <img
                    src="./backend.png"
                    className="img-fluid rounded-start"
                    alt="..."
                  />
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">Back-End Dev</h5>
                    <p className="card-text">
                      Impara a creare infrastrutture server robuste utilizzando
                      linguaggi come Python, Java o Node.js. Approfondisci la
                      gestione dei database, la creazione di API RESTful e la
                      sicurezza delle applicazioni web.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
