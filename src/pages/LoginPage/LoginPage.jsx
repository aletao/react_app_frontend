import { useState, useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import styles from "./LoginPage.module.css";
import { useNavigate } from "react-router-dom";
import { userLogin } from "../../service/AuthService";
import { LoginUser } from "../../models/LoginUser";
import { jwtDecode } from "jwt-decode";
import {useSelector, useDispatch} from "react-redux";
import { setToken, setNome, setCognome, setEmail, setRuoli } from "../../Store/reducers/login/loginReducer";
import Cookies from "js-cookie";

export default function LoginPage() {

  let loggedUser = useSelector((state) => {
    return state.login
  })

  let dispatch = useDispatch();

  const navigate = useNavigate();
  const [emailWrongFormat, setEmailWrongFormat] = useState(true);
  const [loginError, setLoginError] = useState(true);
  const { user, setUser } = useContext(AuthContext);
  const { isAuth, setIsAuth } = useContext(AuthContext);
  const { isAdmin, setIsAdmin } = useContext(AuthContext);
  const [loginUser, setLoginUser] = useState(new LoginUser("", ""));

  const handleChange = (e) => {
    const { name, value } = e.target;
    setLoginUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!isValidEmail(loginUser.email)) {
      setEmailWrongFormat(false);
      console.log("Email non valida");
      return;
    }
    console.log("Email valida:", loginUser.email);

    const response = await userLogin(loginUser);
    if (response.status === 200) {
      setLoginError(true);
      const responseData = await response.json();
      console.log(responseData);

      const token = responseData.token;
      const decoded = jwtDecode(token);

      const ruoli = decoded.ruoli;
      const nome = decoded.nome;
      const cognome = decoded.cognome;
      const email = decoded.email;

      setUser({ nome, cognome, email, ruoli });

      if(ruoli.includes("Admin")){
        setIsAdmin(true);
      }

      /*
      Cookies.set("token", token)
      Cookies.set("nome", nome)
      Cookies.set("cognome", cognome)
      Cookies.set("email", email)
      Cookies.set("ruoli", ruoli) 
      */

      dispatch(setToken(token))
      dispatch(setNome(nome))
      dispatch(setCognome(cognome))
      dispatch(setEmail(email))
      dispatch(setRuoli(ruoli))

      console.log("Token:", token);


      setIsAuth(true);
      navigate("/");
    } else {
      setLoginError(false);
    }
  };

  const isValidEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const handleRegisterClick = () => {
    navigate("/register");
  };

  return (
    <>
      <div className={styles.centerDiv}>
        <img src="./beeloan.png" alt="logo" />
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Email address
            </label>
            <input
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              name="email"
              value={loginUser.email}
              onChange={handleChange}
              aria-describedby="emailHelp"
            />
            <div id="emailHelp" className="form-text" hidden={emailWrongFormat}>
              <span className="text-danger">Formato email non corretto!</span>
            </div>
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              id="exampleInputPassword1"
              name="password"
              value={loginUser.password}
              onChange={handleChange}
            />
            <div>
              <span onClick={handleRegisterClick}>Registrati</span>
            </div>
          </div>
          <div id="emailHelp" className="form-text" hidden={loginError}>
            <span className="text-danger">Email o password errati!</span>
          </div>
          <button type="submit" className="btn btn-primary">
            Accedi
          </button>
        </form>
      </div>
    </>
  );
}
