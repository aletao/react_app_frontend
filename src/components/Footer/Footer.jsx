import styles from "./Footer.module.css";
import { useContext, useEffect } from "react";
import { AuthContext } from "../../contexts/AuthContext";

export default function Footer() {

  const {user, setUser} = useContext(AuthContext);
 
  return (
      <div className={styles.footerStyle}>
        <span style={{color: "black"}}>
          Copryright 2024&copy; Alessandro Taormina
        </span>
      </div>
  );
}