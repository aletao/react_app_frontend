import { useNavigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { NavLink } from "react-router-dom";

export default function Navbar() {
  const navigate = useNavigate();
  const { isAuth, setIsAuth } = useContext(AuthContext);
  const { isAdmin, setIsAdmin } = useContext(AuthContext);

  useEffect(() => {}, [isAuth]);

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top ">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a className="navbar-brand" href="/">
              Beeolan
            </a>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink
                  to="/"
                  className="nav-link"
                  activeclassname="active"
                  aria-current="page">
                  Home
                </NavLink>
              </li>
              <li className="nav-item">
              <NavLink
                  to="/courses"
                  className="nav-link"
                  activeclassname="active"
                  aria-current="page">
                  Courses
                </NavLink>
              </li>
            </ul>

            {isAuth ? (
              <>
                <div className="d-flex">
                  <button
                    className="btn btn-outline-primary"
                    type="submit"
                    onClick={() => {
                      navigate("/myprofile");
                    }}
                  >
                    Profilo utente
                  </button>
                </div>
                {isAdmin && (
                  <div className="d-flex">
                    <button
                      className="btn btn-outline-danger"
                      type="submit"
                      onClick={() => {
                        navigate("/adminpanel");
                      }}
                    >
                      Pannello Admin
                    </button>
                  </div>
                )}
              </>
            ) : (
              <div className="d-flex">
                <button
                  className="btn btn-outline-primary"
                  type="submit"
                  onClick={() => {
                    navigate("/login");
                  }}
                >
                  Login
                </button>
                <button
                  className="btn btn-outline-primary"
                  type="submit"
                  onClick={() => {
                    navigate("/register");
                  }}
                >
                  Register
                </button>
              </div>
            )}
          </div>
        </div>
      </nav>
    </>
  );
}
