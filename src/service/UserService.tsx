import Cookies from "js-cookie";

export const getAllUsers = async (token) => {

    const response = await fetch("http://localhost:8080/api/user/get/all", {
        method: "GET",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            "Authorization": "Bearer " + token
        }
    });

    const responseData = await response.json();
    return responseData;
}

export const deleteUser = async (email, token) => {
        const response = await fetch("http://localhost:8080/api/user/delete/" + email, {
            method: "DELETE",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": "Bearer " + token
            }
        });
    
        const responseData = await response.json();
        return responseData;
    }

export const updateUser = async (user, token) => {
    user.roles = 2;
    const response = await fetch("http://localhost:8080/api/user/update", {
        method: "PUT",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            "Authorization": "Bearer " + token
        },
        body: JSON.stringify(user)
    });

    const responseData = await response.json();
    return responseData;
}