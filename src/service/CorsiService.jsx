export const getAllCorsi = async (token) => {
  const response = await fetch("http://localhost:8080/api/corso/get/all", {
    method: "GET",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
      "Authorization": "Bearer " + token,
    },
  });

  const responseData = await response.json();
  return responseData;
};

export const deleteCourse = async (id, token) => {
  const response = await fetch("http://localhost:8080/api/corso/delete/" + id, {
    method: "DELETE",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
      "Authorization": "Bearer " + token,
    },
  });

  return response;
};
